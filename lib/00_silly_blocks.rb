def reverser
  words = yield.split(' ')
  words.map! { |e| e.reverse }
  words.join(" ")
end

def reverser2
   yield.split(' ').map(&:reverse).join(' ')
end

def adder(param=1)
  yield + param
end

def repeater(n=1)
  n.times do
    yield
  end
end
