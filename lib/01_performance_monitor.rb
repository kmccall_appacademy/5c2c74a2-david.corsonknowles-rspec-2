# # # Topics
# #
# # * stubs
# # * blocks
# # * yield
# #
# # # Performance Monitor
# #
# # This is (a stripped down version of) an actual useful concept: a
# # function that runs a block of code and then tells you how long it
# # took to run.
#

def measure(n = 1)
  initial_time = Time.now
  n.times do
    yield
  end
  end_time = Time.now
  result = (end_time - initial_time) / n
  result
end
