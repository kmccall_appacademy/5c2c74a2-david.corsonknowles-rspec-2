# ### Factors
#
# Write a method `factors(num)` that returns an array containing all the
# factors of a given number.

def factors(num)
  result = []
  1.step(num, 1) do |i|
    result << i if num % i == 0
  end
  result
end

def factors2(number)
  result_array = []
  (1..number).each do |n|
    if number % n == 0
      result_array << n
    end
  end
  result_array
end

# ### Bubble Sort
#
# http://en.wikipedia.org/wiki/bubble_sort
#
# Implement Bubble sort in a method, `Array#bubble_sort!`. Your method should
# modify the array so that it is in sorted order.
#
# > Bubble sort, sometimes incorrectly referred to as sinking sort, is a
# > simple sorting algorithm that works by repeatedly stepping through
# > the list to be sorted, comparing each pair of adjacent items and
# > swapping them if they are in the wrong order. The pass through the
# > list is repeated until no swaps are needed, which indicates that the
# > list is sorted. The algorithm gets its name from the way smaller
# > elements "bubble" to the top of the list. Because it only uses
# > comparisons to operate on elements, it is a comparison
# > sort. Although the algorithm is simple, most other algorithms are
# > more efficient for sorting large lists.
#
# Hint: Ruby has parallel assignment for easily swapping values:
# http://rubyquicktips.com/post/384502538/easily-swap-two-variables-values
#
# After writing `bubble_sort!`, write a `bubble_sort` that does the same
# but doesn't modify the original. Do this in two lines using `dup`.
#
# Finally, modify your `Array#bubble_sort!` method so that, instead of
# using `>` and `<` to compare elements, it takes a block to perform the
# comparison:
#
# ```ruby
# [1, 3, 5].bubble_sort! { |num1, num2| num1 <=> num2 } #sort ascending
# [1, 3, 5].bubble_sort! { |num1, num2| num2 <=> num1 } #sort descending
# ```
#
# #### `#<=>` (the **spaceship** method) compares objects. `x.<=>(y)` returns
# `-1` if `x` is less than `y`. If `x` and `y` are equal, it returns `0`. If
# greater, `1`. For future reference, you can define `<=>` on your own classes.
#
# http://stackoverflow.com/questions/827649/what-is-the-ruby-spaceship-operator

class Array

  def bubble_sort1!  #sorts but doesn't take a proc
    count = 0
    repeat = :on
    while repeat == :on
      self.each_with_index do |e, idx|
        if idx > (self.length - 2)
          next
        end
        if self[idx] > self[idx + 1]
          self[idx], self[idx + 1] = self[idx + 1], e
          count += 1
        end
      end
      if count == 0
        repeat = :off
      end
      count = 0
    end
    self
  end

  def bubble_sort2!(&prc)
    prc ||= Proc.new { |num1, num2| num1 <=> num2 }

    repeat = :on
    while repeat == :on
      count = 0
      self.each_with_index do |e, idx|

        if idx < self.length - 1 && prc.call(self[idx], self[idx + 1]) == 1
          self[idx], self[idx + 1] = self[idx + 1], self[idx]
          count += 1
        end

      end
      if count == 0
        repeat = :off
      end
    end
    self

  end

# works perfectly, so succinct
  def bubble_sort!(&prc)
    prc ||= Proc.new { |num1, num2| num1 <=> num2 }
    loop until self.each_cons(2)
      .with_index.none?{|(n, m),i| self[i], self[i + 1] = m, n if prc
        .call(self[i], self[i + 1]) == 1}
    self
  end

  def bubble_sort3!(&prc)
    prc ||= Proc.new { |num1, num2| num1 <=> num2 }

    iterations = self.size - 2

    return self unless iterations > 0

    swaps = 2

    while swaps > 1 do
      swaps = 0

      0.upto(iterations) do |i|
        if prc.call(self[i], self[i + 1]) == 1
          self[i], self[i + 1] = self[i + 1], self[i]
          swaps += 1
        end
      end

      iterations -= 1
    end

    self
  end

  def bubble_sort(&prc)
    result = self.dup
    result.bubble_sort!
  end

end



# ### Substrings and Subwords
#
# Write a method, `substrings`, that will take a `String` and return an
# array containing each of its substrings. Don't repeat substrings.
# Example output: `substrings("cat") => ["c", "ca", "cat", "a", "at",
# "t"]`.
#
# Your `substrings` method returns many strings that are not true English
# words. Let's write a new method, `subwords`, which will call
# `substrings`, filtering it to return only valid words. To do this,
# `subwords` will accept both a string and a dictionary (an array of
# words).

def substrings(string)
  result = []
  (0...string.length).each do |start|
    (0...string.length).each do |finish|
      result << string[start,finish]
    end
  end
  result.uniq
end

def substrings2(string)
  result = []
  #string.scan(/./) { |match| result << match }
  (1..string.length).each do |size|
    pattern = "." * size
    string.scan(/#{pattern}/) { |match| result << match }
  end
end

def substrings3(string)
  result = []
  how_long = string.length
  (1..how_long).each do |size|
    pattern = "." * size
    string.scan(/#{pattern}/) { |match| result << match }
  end
  result
end

def subwords(words, dictionary)
  test_set = substrings(words)
  dictionary - (dictionary - test_set)
end

def subwords2(words, dictionary)
  substrings(words).select { |e| dictionary.include?(e) }
end

def subwords_iteratively(words, dictionary)
  result = []
  test_set.each do |string|
    if dictionary.include?(string)
      result << string
    end
  end
end


# ### Doubler
# Write a `doubler` method that takes an array of integers and returns an
# array with the original elements multiplied by two.

def doubler(array)
  array.map { |e| e * 2 }
end

# ### My Each
# Extend the Array class to include a method named `my_each` that takes a
# block, calls the block on every element of the array, and then returns
# the original array. Do not use Enumerable's `each` method. I want to be
# able to write:
#
# ```ruby
# # calls my_each twice on the array, printing all the numbers twice.
# return_value = [1, 2, 3].my_each do |num|
#   puts num
# end.my_each do |num|
#   puts num
# end
# # => 1
#      2
#      3
#      1
#      2
#      3
#
# p return_value # => [1, 2, 3]
# ```

class Array

  def my_each(&prc)
    i = 0
    while i < self.length
      prc.call self[i]
      i += 1
    end
    self
  end

end

# class Array
#
#   def my_each(&prc)
#     i = 0
#     while i < self.length
#       yield self[i]
#       i += 1
#     end
#     self
#   end
#
# end


# class Array
#   def my_each
#     if yield.nil?
#       return self
#     end
#     i = 0
#     while i < self.length
#       yield self[i]
#       i += 1
#     end
# end

  # def my_each
  #   self.collect {|e| yield e}
  # end


# class Array
#   def my_each(&prc)
#     i = 0
#     while i < &prc.length
#       self[i] yield
#     #range = 0..(prc.length - 1)
#       i +=1
#     end
#      #prc.scan(/./) self
#   end
# end

#
# while (i < a.length)
#  puts a[i].to_s + "X"
#  i += 1
# end

# ### My Enumerable Methods
# * Implement new `Array` methods `my_map` and `my_select`. Do
#   it by monkey-patching the `Array` class. Don't use any of the
#   original versions when writing these. Use your `my_each` method to
#   define the others. Remember that `each`/`map`/`select` do not modify
#   the original array.
# * Implement a `my_inject` method. Your version shouldn't take an
#   optional starting argument; just use the first element. Ruby's
#   `inject` is fancy (you can write `[1, 2, 3].inject(:+)` to shorten
#   up `[1, 2, 3].inject { |sum, num| sum + num }`), but do the block
#   (and not the symbol) version. Again, use your `my_each` to define
#   `my_inject`. Again, do not modify the original array.

class Array

  def my_map(&prc)
    result = []
    self.my_each do |ele|
      result << prc.call(ele)
    end
    result
  end

  def my_select(&prc)
    result = []
    self.my_each do |e|
      result << e if prc.call(e) == true
    end
    result
  end

  def my_inject(&blk)
    accum = self[0]
    set = self[1..self.length]
    set.my_each do |e|
      accum = blk.call(accum, e)
    end
    accum
  end

end

# ### Concatenate
# Create a method that takes in an `Array` of `String`s and uses `inject`
# to return the concatenation of the strings.
#
# ```ruby
# concatenate(["Yay ", "for ", "strings!"])
# # => "Yay for strings!"
# ```

def concatenate(strings)
  strings.inject(:+) #{ |sum, n| sum + n }
end

def concatenate2(strings)
  strings.inject { |sum, n| sum + n }
end
